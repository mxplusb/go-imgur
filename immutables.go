package go_imgur

const (
	API_URL = "https://api.imgur.com/3/"
	MASHAPE_URL = "https://imgur-apiv3.p.mashape.com/"
	ParseErr = 001
	RefreshErr = 002
	TokenErr = 003
	MissingCredentials = 004
	RateLimit = 005
	AuthErr = 006
	AccountErr = 007
)
