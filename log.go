package go_imgur

import "time"

type Log struct {
	Time        time.Time
	Message     string
	ErrorNumber int
	Method      string
}