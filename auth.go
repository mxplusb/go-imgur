package go_imgur

import (
	"net/http"
	"bytes"
	"sync"
	"encoding/json"
	"fmt"
)

type AuthManager struct {
	*sync.Mutex
	AccessToken        string
	CurrentAccessToken string
	RefreshToken       string
	ClientID           string
	ClientSecret       string
}

type AuthResponse struct {
	AccessToken     string `json:"access_token"`
	RefreshToken    string `json:"refresh_token"`
	ExpiresIn       int `json:"expires_in"`
	TokenType       string `json:"token_type"`
	AccountUsername string `json:"account_username"`
}

func NewAuthWrapper(cid, cis, access, refresh string) (*AuthManager, error) {
	if len(refresh) == 0 {
		return nil, ImgurClientError{
			ErrNum: TokenErr,
			Message: "A refresh token must be provided.",
		}
	}

	return &AuthManager{
		CurrentAccessToken: refresh,
		AccessToken: access,
		ClientID: cid,
		ClientSecret: cis,
	}, nil
}

func (a *AuthManager) Refresh() error {
	data := map[string]string{
		"refresh_token": a.RefreshToken,
		"client_id": a.ClientID,
		"client_secret": a.ClientSecret,
		"grant_type": "refresh_token",
	}

	oauthUrl := API_URL + "oauth2/token"

	var localAuthResponse AuthResponse

	b, err := json.Marshal(data)
	if err != nil {
		return ImgurClientError{
			ErrNum: ParseErr,
			Message: err.Error(),
		}
	}

	cl := http.DefaultClient
	resp, err := cl.Post(oauthUrl, "application/json", bytes.NewBuffer(b))

	if err != nil {
		return err
	} else if resp.StatusCode != 200 {
		err := ImgurClientError{
			APIError: &APIError{
				ErrNum: RefreshErr,
				Message: fmt.Sprintf("Cannot properly refresh token: %s", err),
				HTTPStatus: resp.StatusCode,
			},
		}
		return err
	}

	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&localAuthResponse)
	if err != nil {
		err := ImgurClientError{
			APIError: &APIError{
				ErrNum: ParseErr,
				Message: fmt.Sprintf("Cannot properly parse authentication response. Error: %s", err),
			},
		}
		return err
	}

	a.Lock()
	a.CurrentAccessToken = localAuthResponse.AccessToken
	a.Unlock()
	return nil
}