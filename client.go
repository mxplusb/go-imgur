package go_imgur

import (
	"fmt"
	"net/http"
	"sync"
	"strings"
	"github.com/google/go-querystring/query"
	"encoding/json"
)

type ImgurClient struct {
	*http.Client
	*sync.RWMutex
	AllowedAlbumFields          map[string]struct{}
	AllowedAdvancedSearchFields map[string]struct{}
	AllowedAccountFields        map[string]struct{}
	AllowedImageFields          map[string]struct{}
	ClientID                    string
	ClientSecret                string
	Credits                     *APICredits
	Auth                        *AuthManager
	MashapeKey                  string
}

type APICredits struct {
	UserLimit       int
	UserRemaining   int
	UserReset       string
	ClientRemaining int
	ClientLimit     int
}

func NewClient(cid, cis, access, refresh, mashape string) (*ImgurClient, error) {
	localClient := &ImgurClient{
		ClientID: cid,
		ClientSecret: cis,
		MashapeKey: mashape,
	}

	if len(refresh) > 0 {
		newAuth, err := NewAuthWrapper(cid, cis, access, refresh)
		if err != nil {
			return nil, err
		}
		localClient.Auth = newAuth
	}

	return localClient, nil
}

// QueryBuilder is an abstraction of https://github.com/google/go-querystring/query
func QueryBuilder(data interface{}) (string, error) {
	d, err := query.Values(data)
	if err != nil {
		return nil, err
	}
	return d.Encode()
}

func (ic *ImgurClient) SetUserAuth(access, refresh string) {
	localAuth, err := NewAuthWrapper(ic.ClientID, ic.ClientSecret, access, refresh)
	if err != nil {
		err := ImgurClientError{
			APIError: &APIError{
				Message: fmt.Sprintf("Cannot set user authentication. Error: %s", err),
			},
		}
		return err
	}

	ic.Lock()
	ic.Auth = localAuth
	ic.Unlock()
}

func (ic *ImgurClient) RequestBuilder(method, route string, data []byte, forceAnon bool) (*http.Response, error) {
	var newReq *http.Request
	newReqWithHeaders, err := ic.RequestHeaderBuilder(newReq, forceAnon)
	if err != nil {
		return nil, err
	}

	// leave it to Python to allow for arbitrary complications.
	// https://github.com/Imgur/imgurpython/blob/master/imgurpython/client.py#L128
	var localUrl string
	var localRoute string
	if ic.MashapeKey != nil {
		localUrl = MASHAPE_URL
	} else {
		localUrl = API_URL
	}
	if !strings.Contains(route, "oauth2") {
		localRoute = route
	} else {
		localRoute = "oauth2"
	}

	newReqWithHeaders.Method = method
	newReqWithHeaders.Body = data
	newReqWithHeaders.URL = localUrl + localRoute

	// encode the data as the query string.
	if strings.Contains(method, "post") {

		qString, err := QueryBuilder(data)
		if err != nil {
			return nil, err
		}

		newReqWithHeaders = localUrl + localRoute + qString
	}

	var localCredits APICredits

	resp, err := ic.Do(newReqWithHeaders)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	// TODO this should be a refresh then recursion call.
	if resp.StatusCode == 403 && ic.Auth != nil {
		ic.Auth.Refresh()
		// encode the data as the query string.
		if strings.Contains(method, "post") {

			qString, err := QueryBuilder(data)
			if err != nil {
				return nil, err
			}

			newReqWithHeaders = localUrl + localRoute + qString
		}

		resp, err := ic.Do(newReqWithHeaders)
		defer resp.Body.Close()
		if err != nil {
			return nil, err
		}
	}

	localCredits.ClientRemaining = resp.Header.Get("X-RateLimit-ClientLimit")
	localCredits.UserLimit = resp.Header.Get("X-RateLimit-UserLimit")
	localCredits.UserRemaining = resp.Header.Get("X-RateLimit-UserRemaining")
	localCredits.UserReset = resp.Header.Get("X-RateLimit-UserReset")
	localCredits.ClientLimit = resp.Header.Get("X-RateLimit-ClientLimit")

	ic.Lock()
	ic.Credits = localCredits
	ic.Unlock()

	if resp.StatusCode == 429 {
		return nil, ImgurClientError{
			APIError: &APIError{
				ErrNum: RateLimit,
				Message: "Rate limit exceeded.",
				HTTPStatus: resp.StatusCode,
			},
		}
	}

	return resp, nil
}

func (ic *ImgurClient) RequestHeaderBuilder(headers *http.Request, forceAnon bool) (*http.Request, error) {
	if forceAnon || ic.Auth == nil {
		if ic.ClientID == nil {
			return nil, ImgurClientError{
				ErrNum: MissingCredentials,
				Message: "Client credentials not found!",
			}
		} else {
			headers.Header.Set("Authorization", fmt.Sprintf("Client-ID %s", ic.ClientID))
		}
	} else {
		headers.Header.Set("Authorization", fmt.Sprintf("Bearer %s", ic.Auth.CurrentAccessToken))
	}

	if ic.MashapeKey > 0 {
		headers.Header.Set("X-Mashape-Key", ic.MashapeKey)
	}

	return headers
}

func (ic *ImgurClient) validateUserContext(username string) error {
	if username == "me" && ic.Auth == nil {
		return ImgurClientError{
			Message: "'me' can only be used in the authenticated context.",
			ErrNum: AuthErr,
		}
	}
	return
}