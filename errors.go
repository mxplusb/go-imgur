package go_imgur

type ImgurClientError struct {
	*APIError
	ErrNum  int
	Message string
	error
}

type APIError struct {
	ErrNum     int
	Message    string
	HTTPStatus int
}