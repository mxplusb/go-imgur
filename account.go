package go_imgur

import (
	"encoding/json"
	"fmt"
)

type Account struct {
	Data    struct {
			ID            int `json:"id"`
			URL           string `json:"url"`
			Bio           string `json:"bio"`
			Reputation    float64 `json:"reputation"`
			Created       int `json:"created"`
			ProExpiration bool `json:"pro_expiration"`
		} `json:"data"`
	Status  int `json:"status"`
	Success bool `json:"success"`
}

func (ic *ImgurClient) GetAccount(username string) (Account, error) {
	err := ic.validateUserContext(username)
	if err != nil {
		return Account{}, err
	}

	resp, err := ic.RequestBuilder("GET", fmt.Sprintf("account/%s", username), nil, false)
	defer resp.Body.Close()
	if err != nil {
		return Account{}, ImgurClientError{
			ErrNum: AccountErr,
			APIError: &APIError{
				ErrNum: AccountErr,
				Message: fmt.Sprintf("Error getting account data. Error: %s", err),
			},
		}
	}

	decoder := json.NewDecoder(resp.Body)
	var localAccount Account
	err = decoder.Decode(&localAccount)
	if err != nil {
		return Account{}, ImgurClientError{
			ErrNum: ParseErr,
			Message: fmt.Sprintf("Cannot properly parse account information for %s", username),
		}
	}

	return localAccount, nil
}